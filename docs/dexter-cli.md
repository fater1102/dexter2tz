# How to use Dexter via the command line

## Dexter Contracts

Dexter is a Tezos on-chain, decentralized exchange for the native Tezos token
`tez` (or `XTZ`) and assets built on the Tezos block chain. Currently it supports tokens
from [FA1.2](https://gitlab.com/tzip/tzip/blob/master/proposals/tzip-7/tzip-7.md) and [FA2](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md) contracts.

This tutorial assumes you are familiar with using the Tezos client.

## Originating the Dexter Contract from the Command Line

Dexter is composed of two contracts, an exchange contract and a liquidity token contract, and needs an address of a FA1.2 token contract to originate it.

Clone the dexter repository.

```bash
$ git clone git@gitlab.com:dexter2tz/dexter2tz.git
```

The [LQT contract](../lqt_fa12.mligo.tz) used in Dexter is an example of a simple FA1.2 token.

The dexter contract is at 
[../dexter.mligo.tz](../dexter.mligo.tz).

If you want to compile it yourself, install [ligo](https://ligolang.org/docs/intro/installation) and run:

```
$ ligo compile-contract ../dexter.liquidity_baking.mligo main > ../dexter.liquidity_baking.mligo.tz
```

### Originate an FA1.2 Token Contract

First, lookup the address of the account with which you want to originate the 
FA1.2 token and give ownership of the coins to.

```bash
$ tezos-client list known addresses

simon: tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3 (unencrypted sk known)
bob: tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i (unencrypted sk known)
alice: tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV (unencrypted sk known)
```

I have three accounts on the testnet. I am going to originate a token with
1,000,000 tokens. I will place all of them in Alice's custody. Copy the command 
below and replace `alice` with the name of your account and 
`"tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV"` with your account's address. You 
can change the amount of tokens to whatever number you like. Make sure the two 
numbers match or there will be untransferrable tokens in the contract.

I am calling this token `Tezos Gold`, but there is no storage field in FA1.2 for
the name or the symbol of the token.

```bash
$ tezos-client originate contract tezosGold \
               transferring 0 from alice \
               running path/to/fa1.2.tz \
	   		   --init 'Pair {Elt "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" 100000000000000} {} "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" 100000000000000' \
               --burn-cap 5
```

If you want to originate another contract stored as `tezosGold` in your local 
machine, you can overwrite it by adding `--force` to the end of the command. 
Just remember that it will disassociate the old contract address from 
the symbol, but that contract will still exist on the Tezos blockchain.

Now Alice can transfer Tezos Gold tokens to another account. In the command 
below, the first account address is the owner Alice and the second one is the 
to address Simon. The numerical value is the number of Tezos Gold tokens that 
Alice will send to Simon.

```bash
$ tezos-client call tezosGold \ 
               from alice \
               --entrypoint 'transfer' \
               --arg 'Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" 100' \
               --burn-cap 1
```

Finally, you can check the Tezos Gold balance of each account. This can be done either by inspecting the token big map in storage, as show below, or calling the `getBalance` entrypoint with a callback contract of type `nat`.

```bash
$ tezos-client get contract storage for tezosGold
Pair 4 5 "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" 100000000000000

$ tezos-client hash data '"tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV"' of type address
Raw packed data: 0x050a000000160000471c8882bcf12586e640b7efa46c6ea1e0f4da9e
Script-expression-ID-Hash: expru1LH1CafV3yYgs9BkbrMWWfAE9ye3RdWwyndr9MKYN8w5VQ7Rt
Raw Script-expression-ID-Hash: 0x409c2d6ef28264b346f51c7b28757746eda5d5b2a5407a7462098bbeac018a35
Ledger Blake2b hash: 5MDDaLa2KJ7qo2QkuoNau5uAWNfdqJA4eEoSBVePAA1A
Raw Sha256 hash: 0x2a9dbeb79b0e86f7c524d3f4ee425d08232fa14abef17b1f5bd7ab4b49e42c6a
Raw Sha512 hash: 0xe6bd1f53723c732c3c5e6e2eab426791550d47e66478b888bc10cb515925d372b9b23eb744813620083ec872b0b1208b509cb0a5d85b64a22f10365f7f5eeb23
Gas remaining: 1039934.529 units remaining

$ tezos-client get element expru1LH1CafV3yYgs9BkbrMWWfAE9ye3RdWwyndr9MKYN8w5VQ7Rt of big map 4
99999999999900
```

You can create multiple token contracts. Just copy the first command and replace 
the values as you like.

### Originate the Dexter Exchange Contract

While the token contract is completely standalone, the LQT and exchange contracts are
interdependent (each one needs to store the address of the other one). This interdependency cycle is broken as follows:
- The exchange contract is originated first with a dummy address in its lqtAddress storage field.
- The LQT contract is originated second with the address of the exchange contract in its admin storage field.
- Finally, the manager of the exchange contract resets the lqtAddress in the exchange contract storage to the address of the LQT contract. The manager uses the setLqtAddress entrypoint for that. This entrypoint can only be called once, and only by the manager.

A script is provided with an example origination at [../origination.sh](../origination.sh).

When we originate the Dexter exchange contract, we need to include the 
address of the FA1.2 token contract we just originated. We can look up the 
address in the command line.

```bash
$ tezos-client list known contracts
tezosGold: KT1KxdSabTUDVXvnXkyW7udQvp6FjG9FcrBm
```

Now we can originate the exchange contract. A function is provided in the ligo script to assist with generating the initial storage:

```
$ ligo compile-storage ../dexter2tz/dexter.mligo main \
'build_storage { lqtTotal = 100000000000000n; manager = ("tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" : address); tokenAddress = ("KT1KxdSabTUDVXvnXkyW7udQvp6FjG9FcrBm" : address) }'
> (Pair 0
      (Pair 0
            (Pair 100000000000000
                  (Pair False
                        (Pair False
                              (Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV"
                                    (Pair "KT1KxdSabTUDVXvnXkyW7udQvp6FjG9FcrBm" "tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU")))))))

$ tezos-client originate contract tezosGoldExchange \ 
               transferring 0 from alice \ 
               running path/to/dexter.tz \
               --init '(Pair 0 \
	(Pair 0 \
            (Pair 100000000000000 \
                  (Pair False \
                        (Pair False \
                              (Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" \
                                    (Pair "KT1KxdSabTUDVXvnXkyW7udQvp6FjG9FcrBm" "tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU")))))))' \
               --burn-cap 10
```

Next, we originate the LQT contract. It must have at least one initial liquidity provider since total liquidity can never fall to zero. The address for tezosGoldExchange is provided as the admin.

```
$ tezos-client list known contracts
tezosGoldExchange: KT1NUp9Ko5SaQJbM6Lfu3G9zRgVctavH1ZZ2
tezosGold: KT1KxdSabTUDVXvnXkyW7udQvp6FjG9FcrBm
alice: tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV

$ tezos-client originate contract lqt \ 
               transferring 0 from alice \ 
               running path/to/lqt_fa12.mligo.tz \
               --init 'Pair {Elt "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" 100000000000000} {} "KT1NUp9Ko5SaQJbM6Lfu3G9zRgVctavH1ZZ2" 100000000000000' \
		       --burn-cap 10
```

Next, the initial liquidity provider registered in the LQT contract must actually provide the liquidity to Dexter:

```
$ tezos-client transfer 100000000 from alice to tezosGoldExchange --burn-cap 1

$ tezos-client call tezosGold from alice \
               --entrypoint 'transfer' \
			   --arg 'Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" "KT1NUp9Ko5SaQJbM6Lfu3G9zRgVctavH1ZZ2" 100000000000000' \
			   --burn-cap 1
```

Finally, the manager (alice in our example) must set the LQT address in Dexter:

```
tezos-client list known contracts
> lqt: KT1L4GEr7e2eYxQAytict6Dfou2ZTV2RMY3G
tezosGoldExchange: KT1NUp9Ko5SaQJbM6Lfu3G9zRgVctavH1ZZ2
tezosGold: KT1KxdSabTUDVXvnXkyW7udQvp6FjG9FcrBm
alice: tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV

$ tezos-client call tezosGoldExchange from alice \
               --entrypoint setLqtAddress \
  			   --arg "KT1L4GEr7e2eYxQAytict6Dfou2ZTV2RMY3G"
```

## Adding and Removing Liquidity

Let's add some liquidity. In order to do so, Alice must first approve an 
allowance for Tezos Gold Exchange to spend her Tezos Gold tokens. The first value 
in the parameter pair is the address of whom we want to give an allowance to and 
the second value is the amount of the allowance.

```bash
$ tezos-client call tezosGold \ 
               from alice \
               --entrypoint 'approve' \
               --arg 'Pair "KT1NUp9Ko5SaQJbM6Lfu3G9zRgVctavH1ZZ2" 200' \
               --burn-cap 1
```

In the command below, `transfer 10` means Alice is  adding 10 tez to the 
liquidity pool. The next number is the minimum amount of 
liquidity tokens you want to mint (if that number is not reached it will fail and 
return the tez to you). Then the next number is the maximum 
number of Tezos Gold tokens you want to add to the liquidity pool. When the 
liquidity pool is empty it will add the maximum amount if the account has that 
many tokens. And finally the date is the deadline for which you would like the 
transaction to occur by. 

The values in the parameter are: address of who will own the liquidity from
tezosGoldExchange, the minimum amount of liquidity the user wants to create,
the maximum number of FA1.2 tokens they want to deposit, and the deadline.

```bash
$ tezos-client transfer 10 
               from alice \
               to tezosGoldExchange \
               --entrypoint 'addLiquidity' \
               --arg 'Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" 1 123456 "2030-01-01T12:00:00Z"' \
               --burn-cap 1
```

In this transaction, Alice has initiated the liquidity pool with 10 tez and 100 
Tezos Gold tokens meaning that she has valued Tezos Gold at 10-1. Her ownership
of liquidity in this exchange contract is represented by ownership of liquidity tokens.

We can query her liquidity tokens with the following command.

```bash
$ tezos-client get contract storage for lqt
Pair 6 7 "KT1NUp9Ko5SaQJbM6Lfu3G9zRgVctavH1ZZ2" 100000000000000

# recall we use `tezos-client hash data` to hash big map keys
$ tezos-client get element expru1LH1CafV3yYgs9BkbrMWWfAE9ye3RdWwyndr9MKYN8w5VQ7Rt of big map 6
100000000000000
```

Alice holds 10000000 LQT tokens.

We can also  remove liquidity from the exchange and receive an equivalent amount
of XTZ and Tezos Gold. The first value is the address of who will receive the XTZ
and the Tezos Gold, the second is the amount of LQT you want to get rid of, the
third is the minimum amount of XTZ (in mutez) you want to withdraw (one tez is
1,000,000 mutez), and the fourth is the minimum amount of Tezos Gold tokens you
with to withdraw. If the deadline is passed of if any of the minimums are not met,
then the transaction will fail.

```bash
$ tezos-client call tezosGoldExchange from alice \
               --entrypoint 'removeLiquidity' \
               --arg 'Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" 5000000 1 1 "2030-06-29T18:00:21Z"' \
               --burn-cap 1
```


## Making trades

With another account you can try buying some Tezos Gold tokens from the exchange. 
`transfer 5` means Simon spends one tez. The address is who will receive the 
Tezos Gold tokens, in this case Simon. `5` means he wants to receive at least 1 
Tezos Gold for his five tez and the date is the deadline for the transaction to 
occur. If the deadline has passed or Simon's minimum request has not been met, 
the transaction will fail.

```bash
$ tezos-client transfer 5 \
               from simon \
               to tezosGoldExchange \
               --entrypoint 'xtzToToken' \
               --arg 'Pair "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" 1 "2030-01-29T18:00:00Z"' \
               --burn-cap 1
```

After the xtzToToken transaction, you can confirm Simon's current balance of
Tezos Gold with the following command:

```bash
$ tezos-client get element expruLejqEEe6KzWxgNdRSpwjYRYyqLqyrd16Lyx6ycvw9fZQYxRj3 of big map 4
```

We can also purchase XTZ with Tezos Gold tokens. The first number is the number
of tokens you want to sell, then the minimum amount of XTZ (in mutez) that you
want to receive (one tez is 1,000,000 mutez), and finally the deadline by which 
the transfer should occur.

```bash
$ tezos-client call tezosGoldExchange \
               from simon \
               --entrypoint 'tokenToXtz' \
               --arg 'Pair "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" 40 1 "2030-01-01T12:00:00Z"' \
               --burn-cap 1
```


## Appendix A: FA1.2 Token Contract Commands

### Originate a FA1.2 Token Contract

Reference:
```bash
$ tezos-client originate contract <token-contract-name> \
               transferring 0 from <owner> \
               running path/to/fa1.2.tz \
               --init 'Pair {Elt "<owner>" <token-total-amount>} "<admin>" <token-total-amount>' \
               --burn-cap 5
```

Example:
```bash
$ tezos-client originate contract tezosGold \
               transferring 0 from alice \
               running path/to/fa1.2.tz \
	   		   --init 'Pair {Elt "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" 100000000000000} {} "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" 100000000000000' \
               --burn-cap 5
```

### Transfer FA1.2 Tokens

Reference:
```bash
tezos-client call <token-contract-name> \
             from <sender> \
             --entrypoint 'transfer' \
             --arg 'Pair "<owner>" "<destination>" <value>' \
             --burn-cap 1
```

The sender must be the owner of the tokens or have an allownace from the owner 
of the tokens.

Example:
```bash
$ tezos-client call tezosGold \ 
               from alice \
               --entrypoint 'transfer' \
               --arg 'Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" 100' \
               --burn-cap 1
```

### View FA1.2 Account Balance

Reference:
```bash
$ tezos-client hash data "<owner>" of type address

$ tezos-client get element <hash of owner address> of big map <big map id>
```

Example:
```bash
$ tezos-client get contract storage for tezosGold
Pair 4 5 "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" 100000000000000

$ tezos-client hash data '"tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV"' of type address
Raw packed data: 0x050a000000160000471c8882bcf12586e640b7efa46c6ea1e0f4da9e
Script-expression-ID-Hash: expru1LH1CafV3yYgs9BkbrMWWfAE9ye3RdWwyndr9MKYN8w5VQ7Rt
Raw Script-expression-ID-Hash: 0x409c2d6ef28264b346f51c7b28757746eda5d5b2a5407a7462098bbeac018a35
Ledger Blake2b hash: 5MDDaLa2KJ7qo2QkuoNau5uAWNfdqJA4eEoSBVePAA1A
Raw Sha256 hash: 0x2a9dbeb79b0e86f7c524d3f4ee425d08232fa14abef17b1f5bd7ab4b49e42c6a
Raw Sha512 hash: 0xe6bd1f53723c732c3c5e6e2eab426791550d47e66478b888bc10cb515925d372b9b23eb744813620083ec872b0b1208b509cb0a5d85b64a22f10365f7f5eeb23
Gas remaining: 1039934.529 units remaining

$ tezos-client get element expru1LH1CafV3yYgs9BkbrMWWfAE9ye3RdWwyndr9MKYN8w5VQ7Rt of big map 4
99999999999900
```

### Approve FA1.2 Allowance

Reference:
```bash
$ tezos-client call  <token-contract-name> \
               from <spender> \ 
               --entrypoint 'approve' \ 
               --arg 'Pair "<spender>" <allowance>' \
               --burn-cap 1
```

Example:
```bash
$ tezos-client call tezosGold \
               from alice \ 
               --entrypoint 'approve' \ 
               --arg 'Pair "KT1VbT8n6YbrzPSjdAscKfJGDDNafB5yHn1H" 200' \
               --burn-cap 1
```

## Appendix B: Dexter Exchange

### Originate a Dexter Exchange

See [../origination.sh](../origination.sh).

### Add Liquidity

Reference:
```bash
$ tezos-client transfer <xtz> 
               from <sender> \ 
               to <exchange-contract-name> \
               --arg 'Pair "<owner>" <min_lqt_created> <max_tokens_deposited> "<deadline>"' \
               --burn-cap 1
```

Example:

```bash
$ tezos-client transfer 10 
               from alice \
               to tezosGoldExchange \
               --entrypoint 'addLiquidity' \
               --arg 'Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" 1 123456 "2030-01-01T12:00:00Z"' \
               --burn-cap 1
```

### Remove Liquidity

Reference:
```bash
$ tezos-client call <exchange-contract-name> \
               from <sender> \
               --entrypoint 'removeLiquidity' \
               --arg 'Pair "<to>" <lqt_burned> <min_xtz_withdrawn> <min_tokens_withdrawn> "<deadline>"' \
               --burn-cap 1
```

Example:
```bash
$ tezos-client call tezosGoldExchange from alice \
               --entrypoint 'removeLiquidity' \
               --arg 'Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" 5000000 1 1 "2030-06-29T18:00:21Z"' \
               --burn-cap 1
```

### XTZ to FA1.2 Token

Reference:
```bash
tezos-client transfer <xtz> \
             from <buyer> \ 
             to <exchange-contract-name> \
             --entrypoint 'xtzToToken' \
             --arg 'Pair "<to>" <min-tokens-required> "<deadline>"' \
             --burn-cap 1
```

Example:
```bash
$ tezos-client transfer 5 \
               from simon \
               to tezosGoldExchange \
               --entrypoint 'xtzToToken' \
               --arg 'Pair "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" 1 "2030-01-29T18:00:00Z"' \
               --burn-cap 1
```

### FA1.2 Token to XTZ

Reference:
```bash
$ tezos-client call <exchange-contract-name> \
               from <sender> \
               --entrypoint 'tokenToXtz' \
               --arg 'Pair "<to>" <tokens_sold> <min_xtz_bought> "<deadline>"' \
              --burn-cap 1
```

Example:
```bash
$ tezos-client call tezosGoldExchange \
               from simon \
               --entrypoint 'tokenToXtz' \
               --arg 'Pair "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" 40 1 "2030-01-01T12:00:00Z"' \
               --burn-cap 1
```

### FA1.2 Token to Token

Used to trade the token with another FA1.2 or FA2 token by calling another Dexter contract.

Reference:
```bash
$ tezos-client call <exchange-contract-name> \
               from <sender> \
               --entrypoint 'tokenToToken' \
               --arg 'Pair "<outputDexterContract>" <minTokensBought> "<owner>" "<to>" "<deadline>"' \
               --burn-cap 1
```

Example:
```bash
$ tezos-client call tezosGoldExchange \
               from simon \
               --entrypoint 'tokenToToken' \
               --arg 'Pair "KT1QcD2WsVVqk1nknUm6wyErS2h329K59T42" 1 "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" 1000 "2030-01-01T12:00:00Z"' \
               --burn-cap 1
```

### Default 

Give free XTZ to a dexter contract, e.g. baking rewards.

Example:
```bash
$ tezos-client transfer 5 from simon \
               to tezosGoldExchange \
               --entrypoint 'default' \
               --arg 'Unit' \
               --burn-cap 1
```

### Get Exchange Pool Balances

Reference:
```bash
$ tezos-client get contract storage for <exchange-contract-name>
```

$ Example:
```bash
$ tezos-client get contract storage for tezosGoldExchange

Pair 9995017483785
     10005000000
     100000000000000
     False
     False
     "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
     "KT1QJf8iHcNSmheDF3V7yr524gckAC88LV2G"
     "tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU"

9995017483785 Tezos Gold
10005 tez
100000000000000 liquidity tokens
```
