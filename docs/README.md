# Overview of the documentation


- [An informal specification of Dexter 2](informal-spec/dexter2.md)
- [A token integration checklist](token-integration.md)
- [How to integrate a Dexter exchange into your application](dexter-integration.md)
- [How to use Dexter via the command line](dexter-cli.md)
